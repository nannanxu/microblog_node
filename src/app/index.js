const Koa = require('koa')
const bodyParser = require("koa-bodyparser")
const errorHandler = require("../app/error-handle")
const router = require("../router/index")

const app = new Koa()

app.use(bodyParser())
router(app)

app.on('error',errorHandler)


module.exports = app