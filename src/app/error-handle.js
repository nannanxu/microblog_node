const errorTypes = require("../constants/error-types")

const errorHandler = (ctx , error) => {
  let status,message;
   switch (error.message){
       case  errorTypes.NAME_OR_PASSWORD_IS_REQUIRED:
         status = 400;
         message = "用户名或者密码不能为空~";
         break;

       case errorTypes.NAME_IS_EXSITS:
         status = 400;
         message = '用户已经存在～';
         break; 
         
      case errorTypes.USER_DOES_NO_EXISTS:
         status = 404;
         message = '用户不存在～';
         break;

      case errorTypes.PASSWORD_IS_INCORRENT:
        status = 404;
        message = "用户密码不一致～"
        break;

        case errorTypes.UNAUTHORIZATION:
          status = 401;
          message = "token无效～"
          break;
        case errorTypes.UNPERMISSION:
        status = 404;
        message = "用户没有权限～"
        break
        
        case errorTypes.UNISPERMISSION :
          status = 404;
          message = "token解析错误"
          break

         default:
             status = 400;
           message = "NOT FOUND"
   }

  ctx.status = status;
  ctx.body = message;

}

module.exports =  errorHandler
