const dotenv = require("dotenv")
const fs = require("fs")
const path = require("path")

dotenv.config()

// token
const public_key = fs.readFileSync(path.resolve(__dirname,"../keys/public.key"));
const private_key = fs.readFileSync(path.resolve(__dirname,"../keys/private.key"));


module.exports = {
    APP_PORT,
    MYSQL_HOST, 
    MYSQL_PORT ,
    MYSQL_DATABASE, 
    MYSQL_USER ,
    MYSQL_PASSWORD,
    APP_HOST,
} = process.env

module.exports.public_key = public_key
module.exports.private_key = private_key