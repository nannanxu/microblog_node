const jwt = require("jsonwebtoken")

const errorType = require("../constants/error-types");
const md5password = require("../utils/password-handle");
const Authservice = require("../service/auth.services")
const service = require("../service/user.services");
const { public_key } = require("../app/config");

const verifyLogin = async(ctx,next) =>{
 // 1.获取用户名和密码
 const { name,password } = ctx.request.body;


 // 2.判断用户名或者密码不为空
 if(!name || !password || name === "" || password === ""){
     const error = new Error(errorType.NAME_OR_PASSWORD_IS_REQUIRED)
     return ctx.app.emit('error',ctx, error)

 }

// 3.判断用户是否存在
 const rusult = await service.getUserByName(name)
 const user = rusult[0]
 if(!user){
     const error = new Error(errorType.USER_DOES_NO_EXISTS)
     return ctx.app.emit('error',ctx, error)
 }

 // 4.判断密码是否和数据库中的密码一致(加密)
 if(md5password(password) != user.password){
     const error = new Error(errorType.PASSWORD_IS_INCORRENT)
     return ctx.app.emit('error',ctx,error)
 }
 ctx.user = user
 await next()
}

const verifyAuth = async(ctx,next) => {
  // 获取token
  const authorization = ctx.headers.authorization;
  if(!authorization){
   const error = new Error(errorType.UNAUTHORIZATION);
   return ctx.app.emit('error',ctx,error)
  }
  const token = authorization.replace("Bearer " , "");
  try{
      // 这个result 里面包含了用户的id name 还有各种信息
      const result = jwt.verify(token,public_key,{
          algorithms:["RS256"], 
      });
      ctx.user = result;
      await next();
  }catch(err){
      console.log(err.message);
     const error = new Error(errorType.UNAUTHORIZATION);
     return ctx.app.emit('error',ctx,error)
  }
}





const verifyPermission = async(ctx,next) =>{
    // 验证token是否生效

     // 1.获取参数
     const [resourceKey] = Object.keys(ctx.params);
     const tableName = resourceKey.replace("Id","");
     const resourceId = ctx.params[resourceKey];
     const { id } = ctx.user;
     try{
     const isPermission =  await Authservice.checkResource(tableName,id,resourceId)
     await next()
     if(!isPermission) throw new Error()
    }catch{
        console.log(123);
    const error = new Error(errorType.UNISPERMISSION);
    return ctx.app.emit('error',ctx,error)
    }
}

module.exports = {
    verifyLogin,
    verifyAuth,
    verifyPermission
}