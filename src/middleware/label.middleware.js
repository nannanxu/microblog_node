const service = require("../service/label.services")


const verifyLabelExists = async(ctx,next) => {
    // 1.取出所有要添加的标签
    const {labels} = ctx.request.body;
 
    // 2.判断每一个标签是否存在
    const isExistLabel = []
    for(let name of labels){
      const labelResult = await service.getLabelByName(name);
      const label = {name};
      if(!labelResult){
          const result = await service.create(name);
          label.id = result.insertId;
      }else{
          label.id = labelResult.id;
      }
      isExistLabel.push(label);
    }
    ctx.labels = isExistLabel;
    await next();
}

module.exports = {
    verifyLabelExists
}