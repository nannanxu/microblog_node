const connection = require("../app/database")

class fileServices{
    async createAvatar(filename,mimetype,size,id){ 
         const statement = `INSERT INTO avatar (filename,mimetype,size,user_id) VALUES (?,?,?,?);`;
         const [result] = await connection.execute(statement,[filename,mimetype,size,id]);
         return result;
    }
    async getAvatarByUserId(userId){
        const  statement = `SELECT * FROM avatar WHERE user_id = ?;`;
        const [result] = await  connection.execute(statement,[userId]);
        return result.pop();
    }

    async createFile(filename,mimetype,size,id,momentId){
        console.log(momentId);
        try{const statement = `INSERT INTO avatar (filename,mimetype,size,user_id,momentId) VALUES (?,?,?,?,?);`;const [result] = await connection.execute(statement,[filename,mimetype,size,id,momentId]);
        return result}
        catch(err){
            console.log(err);
        }
    }
}

module.exports = new fileServices()