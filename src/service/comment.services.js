const connection = require("../app/database");


class commentServices{
    async create (id,content,momentId){
        console.log([content,momentId,id]);
      const statment = `INSERT  INTO comment  (content,moment_id,user_id) VALUES (?,?,?);`;
      try{const [result] = await connection.execute(statment,[content,momentId,id])
      return result}
      catch(err){
      }
    }

    async reply(id,momentId,content,commentId){
       const statement = `INSERT comment (content,moment_id,user_id,comment_id) 
       VALUES(?,?,?,?);`;
       const [result] = await connection.execute(statement,[content,momentId,id,commentId])
       return result

    }

    async update(commentId,content){
        const statement = `UPDATE comment SET content = ? WHERE id = ?;`;
        const [result] = await connection.execute(statement,[content,commentId]);
        return result
    }

    async remove(commentId){
        const statement = `DELETE FROM comment WHERE id = ? ;`;
        const [result] = await connection.execute(statement,[commentId]);
        return  result 
    }

    async list(momentId){
      console.log(momentId);
      const statement = `SELECT c.id,c.content,c.comment_id commentId, c.createAt createTime,
                          JSON_OBJECT("id",u.id,'name',u.name) AS users
                          FROM comment c
                          LEFT JOIN users u ON u.id = c.user_id
                          WHERE moment_id = ? 
      ;`
      const [ result ] = await connection.execute(statement,[momentId]);
      return result
    }
}


module.exports = new commentServices()