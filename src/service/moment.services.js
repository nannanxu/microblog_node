const connections = require("../app/database")

// const sqlFragment = `
// SELECT 
// m.id id,m.content content, m.createAt createtime,m.updateAt updatetime,
// JSON_OBJECT("id",u.id,"name",u.name) AS author
// FROM moment AS m LEFT JOIN users  AS u on m.user_id = u.id`


class MomentService {
    async create(userid, content) {
        const statement = `INSERT INTO moment (user_id,content) VALUES (?,?);`;
        const result = await connections.execute(statement, [userid, content])
        return result[0]
    }

    async getMomentId(momentId) {
      console.log(momentId);
       const statement = ` 
       SELECT 
        m.id id, m.content content, m.createAt createTime, m.updateAt updateTime,
        JSON_OBJECT('id', u.id, 'name', u.name, 'avatarUrl', u.avatar_url) author,
        IF(COUNT(l.id),JSON_ARRAYAGG(
          JSON_OBJECT('id', l.id, 'name', l.name)
        ),NULL) labels,
        (SELECT IF(COUNT(c.id),JSON_ARRAYAGG(
          JSON_OBJECT('id', c.id, 'content', c.content, 'commentId', c.comment_id, 'createTime', c.createAt,
                      'user', JSON_OBJECT('id', cu.id, 'name', cu.name, 'avatarUrl', cu.avatar_url))
        ),NULL) FROM comment c LEFT JOIN users cu ON c.user_id = cu.id WHERE m.id = c.moment_id) comments,
        (SELECT JSON_ARRAYAGG(CONCAT('http://localhost:9090/moment/images/', file.filename)) 
        FROM file WHERE m.id = file.moment_id) images
      FROM moment m
      LEFT JOIN users u ON m.user_id = u.id
      LEFT JOIN moment_label ml ON m.id = ml.moment_id
      LEFT JOIN label l ON ml.label_id = l.id
      WHERE m.id = ?
      GROUP BY m.id;
        `;
        try{const result = await connections.execute(statement, [momentId])
        return result[0]
        }catch(err){
          console.log(err.message);
        }
  }
    async getMomentList(offset, size) {
        const statement = `
        SELECT 
        m.id id,m.content content, m.createAt createtime,m.updateAt updatetime,
        JSON_OBJECT("id",u.id,"name",u.name) AS author,
				(SELECT COUNT(*) FROM comment c WHERE  c.moment_id = m.id ) AS commentCount,
        (SELECT COUNT(*) FROM moment_label ml WHERE ml.moment_id = m.id) labelCount,
        (SELECT JSON_ARRAYAGG(CONCAT('http://localhost:8000/moment/images/', file.filename)) 
        FROM file WHERE m.id = file.moment_id) images
        FROM moment AS m LEFT JOIN users  AS u on m.user_id = u.id
        LIMIT ?, ?;`;
        const result = await connections.execute(statement, [offset, size])
            return result[0]

        
    }

    async updateMomentList(content, momentId) {
        const statement = `UPDATE moment SET  content = ? WHERE id = ?`
        const result = await connections.execute(statement, [content, momentId])
        return result
    }

    async removeMomentList(momentId) {
        const statement = `DELETE FROM moment WHERE id = ?;`
        const [result] = await connections.execute(statement, [momentId])
        return result
    }

    async getFileByFilename(filename){
      const statement = `SELECT * FROM file WHERE filename = ?`;
      const [result] = await connections.execute(statement,[filename]);
      return result[0];
    }
}


module.exports = new MomentService();