const connections = require("../app/database")

class Authservice{
    async checkResource(tableName,resourceId,id){
        const statement = `SELECT * FROM ${tableName} WHERE id = ? AND user_id= ?;`;
        const [result] = await connections.execute(statement,[id,resourceId])
        return result.length === 0 ? false : true;
    }
}


module.exports = new Authservice();