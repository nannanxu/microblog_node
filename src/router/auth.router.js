const Router = require("koa-router")
const{
   login 
}  = require("../controller/auth.controller.js")

const {
    verifyLogin,
    verifyAuth
} = require("../middleware/auth.middleware")


const loginRouter = new Router()


loginRouter.post("/login",verifyLogin,login)
loginRouter.post("/test",verifyAuth,login)

module.exports = loginRouter