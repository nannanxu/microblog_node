const Router = require("koa-router")

const momentRouter= new Router({prefix:"/moment"})

const {
    verifyLabelExists
} = require("../middleware/label.middleware")

const {
    create,
    detail,
    list,
    update,
    remove,
    fileInfo
} = require("../controller/moment.controller")

const {
    addLabel
} = require("../controller/label.controller")

const {
    verifyAuth,
    verifyPermission,
} = require("../middleware/auth.middleware")


momentRouter.post('/',verifyAuth,create);
momentRouter.get("/:momentId",detail)
 momentRouter.get("/",list);

momentRouter.delete("/:momentId",verifyAuth,verifyPermission,remove);
momentRouter.patch("/:momentId",verifyAuth,verifyPermission,update);

momentRouter.post("/:momentId/labels" ,verifyAuth,verifyPermission,verifyLabelExists,addLabel);

momentRouter.get("/images/:filename", fileInfo)


module.exports = momentRouter;