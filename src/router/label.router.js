const Router = require("koa-router");

const labelRouter = new Router({prefix:"/label"});

const {
    create,
    list,
} = require("../controller/label.controller")

const {
    verifyPermission
} = require("../middleware/auth.middleware")


labelRouter.post("/",create);
labelRouter.get("/",list);

module.exports = labelRouter;