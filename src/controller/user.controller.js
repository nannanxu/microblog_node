const fileServices = require("../service/file.services");
const service = require("../service/user.services");
const {AVATAR_PATH} = require("../constants/file-path")
const fs = require("fs")

class UserController {
    async create(ctx,next) {
        // 获取用户请求传递的参数
        const user = ctx.request.body;
        // 查询数据
        const result = await service.create(user);

        // 返回数据
        ctx.body = result
    }

    async avatarInfo(ctx,next){
        const {userId} = ctx.params;
        
       console.log(userId);
        const avatarInfo = await fileServices.getAvatarByUserId(userId);
        ctx.response.set('content-type', avatarInfo.mimetype)
        ctx.body = fs.createReadStream(`${AVATAR_PATH}/${avatarInfo.filename}`)
    
    }
}

module.exports = new UserController()
