const services = require("../service/label.services");
const { list } = require("./moment.controller");

class labelController{
    async create(ctx,next){
        const {name} = ctx.request.body
        const result = await services.create(name)
        
       return ctx.body = result
    }

    async addLabel(ctx,next){
        const {labels} = ctx;
        const {momentId} = ctx.params;
        
        
        for(let label of labels){
        const isExist = await services.hasLabel(momentId,label.id);
        console.log(isExist);
        console.log(label.id);
        if (!isExist){
            await services.addLabel(momentId,label.id)
        }
        ctx.body = "添加标签成功";
    }
}
     async list(ctx,next){
         const {offest,limit} = ctx.query;
         const result = await services.getLabelList(limit,offest)
         ctx.body = result
     }
}

module.exports = new labelController()