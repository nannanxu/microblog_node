const fs = require('fs')
const momentService = require("../service/moment.services")
const {PICTURE_PATH} = require("../constants/file-path")

class MomentController {
    async create(ctx,next){
        const userid = ctx.user.id;
        const content = ctx.request.body.content;
        const result = await momentService.create(userid,content);
        ctx.body = result;
    }

    async detail(ctx,next){
        // 1.获取数据(momentId)
        const {momentId} = ctx.params;
    
        //2.根据id去查询这条数据
        const result = await momentService.getMomentId(momentId)
        ctx.body = result;
    }

    async list(ctx,next){
        // 1.获取offset 获取size
        const {offset,size} = ctx.query;

        // 2.查询列表
        const result = await momentService.getMomentList(offset, size);
        ctx.body = result;
    }

    async update(ctx,next){
        // 1.获取参数
        const {content} = ctx.request.body;
        const {momentId} = ctx.params;

        // 2.进行修改
        const result = await momentService.updateMomentList(content,momentId);
        ctx.body = '修改成功' + `文字${content}` 
    }

    async remove(ctx,next){
        const{momentId} = ctx.params;
        const result = await momentService.removeMomentList(momentId)
        ctx.body = result
    }
    
    async fileInfo(ctx,next){
        const {filename} = ctx.params;
        const result = await momentService.getFileByFilename(filename);

        ctx.response.set('content-type',fileInfo,mimetype); 
        ctx.body = fs.createReadStream(`${PICTURE_PATH}/${filename}`)

    }
}


module.exports = new MomentController();