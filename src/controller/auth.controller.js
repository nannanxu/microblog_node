const jwt = require("jsonwebtoken");
const {private_key} = require("../app/config.js")

class authController{
  async login(ctx,next){
      const {id,name} = ctx.user;
     const token = jwt.sign({id,name} , private_key,{
       expiresIn:60 * 60 * 24,
       algorithm:"RS256"
     });
  
     ctx.body = {id , name , token}
      // ctx.body = `登陆成功！欢迎${name}用户回来`
  }
}

module.exports = new authController()