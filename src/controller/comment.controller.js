const service = require("../service/comment.services")

class commentController{
    async create(ctx,next){
      const {id} = ctx.user;
      const {content,momentId} = ctx.request.body;
      const result =  await service.create(id,content,momentId);

    ctx.body = result
    }

    async reply(ctx,next){
      const {id} = ctx.user;
      const {commentId} = ctx.params
      const {content,momentId} = ctx.request.body;
      const result = await service.reply(id,momentId,content,commentId)

      ctx.body = result
    }
    
    async update(ctx,next){
      const {commentId} = ctx.params;
      const  {content} = ctx.request.body;
      const result = await service.update(commentId,content);

      ctx.body = result
    }

    async remove(ctx,next){
        const {commentId} = ctx.params;
        console.log(123);
        const result = await service.remove(commentId)
        ctx.body = result
    }
    
    async list(ctx,next){
      const {momentId} = ctx.query;
      const result = await service.list(momentId);
      ctx.body = result
    }
}


module.exports = new commentController()